<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Inicio | Chronnooss</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- App css -->
    <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" id="light-style">
    <link href="assets/css/app-dark.min.css" rel="stylesheet" type="text/css" id="dark-style">

    <!-- Custom css -->
    <link href="assets_base/css/styles.css" rel="stylesheet" type="text/css">

</head>

<body class="loading"
    data-layout-config='{"leftSideBarTheme":"dark","layoutBoxed":false, "leftSidebarCondensed":false, "leftSidebarScrollable":false,"darkMode":false, "showRightSidebarOnStart": true}'>
    <!-- Begin page -->
    <div class="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
        <?php
        
            include "paginas/left-sidebar.php";
        
        ?>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                <!-- Topbar Start -->
                <?php
                
                    include "paginas/topbar.php";
                
                ?>
                <!-- end Topbar -->

                <!-- Start Content-->
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box">
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Chronnooss</a></li>
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Bibliotecas</a></li>
                                        <li class="breadcrumb-item active">Pokémon</li>
                                    </ol>
                                </div>
                                <h4 class="page-title">Biblioteca de Pokémon</h4>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <h4 class="header-title">Pokémon</h4>

                                        <form id="formCatalogoPokemon">
                                            <div class="row g-2">
                                                <div class="mb-3 col-xs-12 col-md-6">
                                                    <label for="txtBusquedaPokemon" class="form-label">Búsqueda</label>
                                                    <input type="text" class="form-control" id="txtBusquedaPokemon"
                                                        placeholder="Ingresa lo que desees buscar..">
                                                </div>
                                                <div class="mb-3 col-xs-12 col-md-3">
                                                    <p class="mb-1 fw-bold text-muted">Tipo(s)</p>
                                                        <select class="form-control select2" id="selTipos" data-toggle="select2">
                                                            <option value="0">Todos los tipos</option>
                                                            <option value="1">Fuego</option>
                                                            <option value="2">Agua</option>
                                                            <option value="3">Planta</option>
                                                            <option value="4">Eléctrico</option>
                                                            <option value="5">Roca</option>
                                                            <option value="6">Tierra</option>
                                                            <option value="7">Veneno</option>
                                                            <option value="8">Bicho</option>
                                                            <option value="9">Siniestro</option>
                                                            <option value="10">Hada</option>
                                                            <option value="11">Psíquico</option>
                                                            <option value="12">Fantasma</option>
                                                            <option value="13">Luchador</option>
                                                            <option value="14">Hielo</option>
                                                            <option value="15">Volador</option>
                                                            <option value="16">Dragón</option>
                                                            <option value="15">Acero</option>
                                                            <option value="16">Normal</option>
                                                        </select>

                                                </div>
                                                <div class="mb-3 col-xs-12 col-md-3">
                                                    <p class="mb-1 fw-bold text-muted">Generación</p>
                                                        <select class="form-control select2" id="selGeneraciones" data-toggle="select2">
                                                            <option value="0">Toda(s) las generaciones</option>
                                                            <option value="1">Primera generación</option>
                                                            <option value="2">Segunda generación</option>
                                                            <option value="3">Tercera generación</option>
                                                            <option value="4">Cuarta generación</option>
                                                            <option value="5">Quinta generación</option>
                                                            <option value="6">Sexta generación</option>
                                                            <option value="7">Séptima generación</option>
                                                            <option value="8">Octava generación</option>
                                                            
                                                        </select>

                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary">Buscar</button>
                                        </form>
                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">   
                                        <h4 class="header-title">Primera generación</h4>         
                                        <div class="table-responsive">
                                            <table id="tblCatalogoPokedex" class="table table-hover table-centered mb-0">
                                                <thead>
                                                    <tr>
                                                        <!-- <th>Id</th> -->
                                                        <th>Número</th>
                                                        <th>Sprite</th>
                                                        <th>Shiny</th>
                                                        <th>Nombre</th>
                                                        <th>Tipo(s)</th>
                                                        <th>Artwork</th>
                                                        <th>Hp</th>
                                                        <th>Attack</th>
                                                        <th>Defense</th>
                                                        <th>Special Attack</th>
                                                        <th>Special Defense</th>
                                                        <th>Speed</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tblCatalogoPokedexBody">
                                                    
                                                </tbody>
                                            </table>
                                        </div> <!-- end table-responsive-->
                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                        
                    </div>
                </div> <!-- container -->

            </div> <!-- content -->

            <!-- Footer Start -->
            <?php
            
                include "paginas/footer.php";
            
            ?>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->


    <!-- Right Sidebar -->
    <?php
    
        include "paginas/right-sidebar.php";
    
    ?>
    <!-- /End-bar -->

    <!-- Template -->
    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <!-- Plugins -->
    <!-- -- Rating -->
    <script src="assets/js/vendor/jquery.rateit.min.js"></script>
    <!-- Checkbox -->
    <script src="assets/js/vendor/jquery.dataTables.min.js"></script>
    <script src="assets/js/vendor/dataTables.bootstrap5.js"></script>
    <script src="assets/js/vendor/dataTables.responsive.min.js"></script>
    <script src="assets/js/vendor/responsive.bootstrap5.min.js"></script>
    <script src="assets/js/vendor/dataTables.checkboxes.min.js"></script>

    <!-- Jquery -->
    <script src="assets_base/js/plugins/jquery/jquery.min.js"></script>
    
    <!-- Ajax -->
    <script src="assets_base/js/ajax/catalogo_pokemon.ajax.js"></script>

    <!-- Custom -->
    <script src="assets_base/js/main.js"></script>
</body>

</html>