<div class="leftside-menu">

    <!-- LOGO -->
    <a href="index.php" class="logo text-center logo-light">
        <span class="logo-lg">
            <img src="assets/images/logo.png" alt="" height="16">
        </span>
        <span class="logo-sm">
            <img src="assets/images/logo_sm.png" alt="" height="16">
        </span>
    </a>

    <!-- LOGO -->
    <a href="index.php" class="logo text-center logo-dark">
        <span class="logo-lg">
            <img src="assets/images/logo-dark.png" alt="" height="16">
        </span>
        <span class="logo-sm">
            <img src="assets/images/logo_sm_dark.png" alt="" height="16">
        </span>
    </a>

    <div class="h-100" id="leftside-menu-container" data-simplebar="">

        <!--- Sidemenu -->
        <ul class="side-nav">

            <li class="side-nav-title side-nav-item">Navegación</li>

            
            <li class="side-nav-item">
                <a href="index.php" class="side-nav-link">
                    <i class="uil-home-alt"></i>
                    <span> Inicio </span>
                </a>
            </li>

            <li class="side-nav-title side-nav-item">Bibliotecas</li>

            <li class="side-nav-item">
                <a href="catalogo_videojuegos.php" class="side-nav-link">
                    <i class="uil-clipboard-alt"></i>
                    <span> Videojuegos </span>
                </a>
            </li>

            <li class="side-nav-item">
                <a href="catalogo_pokemon.php" class="side-nav-link">
                    <i class="uil-clipboard-alt"></i>
                    <span> Pokémon </span>
                </a>
            </li>

            <li class="side-nav-title side-nav-item">Mis páginas</li>

            <li class="side-nav-item">
                <a href="#" class="side-nav-link">
                    <i class="uil-clipboard-alt"></i>
                    <span> Equipos Pokémon </span>
                </a>
            </li>

        </ul>

        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>